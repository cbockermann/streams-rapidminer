package stream.mining;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.Callable;

import stream.io.SourceURL;

import com.rapidminer.Process;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.tools.XMLException;

/**
 * @author Hendrik Blom
 * 
 */
public class RapidMinerProcess implements Callable<Boolean> {

	private SourceURL url;
	private Map<String, String> macros;

	public RapidMinerProcess(SourceURL url, Map<String, String> macros) {
		this.url = url;
		this.macros = macros;
	}

	@Override
	public Boolean call() throws Exception {
		com.rapidminer.Process p;
		try {
			InputStream in = url.openStream();
			p = new Process(in);
			p.run(null, 1, macros);
			p.stop();
		} catch (IOException | XMLException | OperatorException e1) {
			e1.printStackTrace();
			return false;
		}
		return true;
	}

}
