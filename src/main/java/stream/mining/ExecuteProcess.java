package stream.mining;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.expressions.version2.SourceURLExpression;
import stream.expressions.version2.StringArrayExpression;
import stream.expressions.version2.StringExpression;
import stream.io.SourceURL;

/**
 * @author Hendrik Blom
 * 
 */
public class ExecuteProcess extends AbstractProcessor {

	final static Logger log = LoggerFactory.getLogger(ExecuteProcess.class);

	private StringExpression pid;
	private SourceURLExpression processUrl;
	private ExecutorService pool;
	private StringArrayExpression macros;

	public StringExpression getPid() {
		return pid;
	}

	public void setPid(StringExpression pid) {
		this.pid = pid;
	}

	public SourceURLExpression getProcessUrl() {
		return processUrl;
	}

	public void setProcessUrl(SourceURLExpression file) {
		this.processUrl = file;
	}

	public StringArrayExpression getMacros() {
		return macros;
	}

	public void setMacros(StringArrayExpression macros) {
		this.macros = macros;
	}

	@Override
	public void init(ProcessContext context) throws Exception {
		super.init(context);
		pool = Executors.newFixedThreadPool(1);
		RapidMiner
				.initialize(com.rapidminer.RapidMiner.ExecutionMode.EMBEDDED_WITHOUT_UI);
	}

	@Override
	public Data process(Data data) {
		SourceURL surl;
		try {
			surl = processUrl.get(this.context, data);

			Map<String, String> ms = new HashMap<String, String>();
			String[] mms = macros.get(data);
			for (String k : mms) {
				Serializable s = data.get(k);
				if (s != null)
					ms.put(k, s.toString());
			}
			RapidMinerProcess p = new RapidMinerProcess(surl, ms);
			Future<Boolean> f = pool.submit(p);
			f.get();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	@Override
	public void finish() throws Exception {
		List<Runnable> await = pool.shutdownNow();
		for (Runnable r : await) {
			RapidMinerProcess rmp = (RapidMinerProcess) r;
			log.info(rmp.toString());
		}

	}

}
