/**
 * 
 */
package stream.mining;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.io.SourceURL;
import stream.mining.utils.ExampleSetFactory;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.AttributeRole;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.AbstractIOObject;
import com.rapidminer.operator.learner.PredictionModel;

/**
 * @author chris
 * 
 */
public class ApplyModel extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(ApplyModel.class);
	protected SourceURL model;
	protected PredictionModel predictionModel;
	protected String modelName;
	protected boolean confidence;

	public ApplyModel() {
		modelName = "";
		confidence = true;
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		modelName = "@prediction"
				+ (modelName == null || modelName.isEmpty() ? "" : ":"
						+ modelName);

		log.info("Loading RM PredictionModel from {}", model);

		try {
			InputStream b = model.openStream();
			ObjectInputStream ois = new ObjectInputStream(b);
			predictionModel = (PredictionModel) ois.readObject();
			log.info("   loaded model: {}", predictionModel);
			ois.close();
		} catch (Exception e) {

			try {
				RapidMiner.initialize();

				log.info("Trying to read model using AbstractIOObject...");

				AbstractIOObject.InputStreamProvider provider = new AbstractIOObject.InputStreamProvider() {
					@Override
					public InputStream getInputStream() throws IOException {
						return model.openStream();
					}
				};

				predictionModel = (PredictionModel) AbstractIOObject
						.read(provider);
				log.info("Model ready:{} !", predictionModel.toString());
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		if (model == null) {
			log.warn("No model loaded!");
			return input;
		}

		try {
			final ArrayList<Data> items = new ArrayList<Data>();
			items.add(input);
			ExampleSet examples = ExampleSetFactory.createExampleSet(items);

			examples = predictionModel.apply(examples);
			Attribute predictionAttribute = examples.getAttributes()
					.getPredictedLabel();
			log.debug("predictionAttribute is: {}", predictionAttribute);

			if (predictionAttribute.isNominal()) {
				String label = examples.getExample(0).getNominalValue(
						predictionAttribute);
				input.put(modelName, label);
				log.debug("Label is: {}", label);
			} else {
				Double label = examples.getExample(0).getNumericalValue(
						predictionAttribute);
				input.put(modelName, label);
				log.debug("Label is: {}", label);
			}

			Attributes attributes = examples.getAttributes();
			Iterator<AttributeRole> special = attributes.specialAttributes();

			if (confidence) {
				while (special.hasNext()) {
					AttributeRole role = special.next();
					String name = role.getSpecialName();
					log.debug("Special attribute '{}'", name);

					if (name.startsWith("confidence_")) {
						input.put(
								"@confidence:"
										+ name.replaceFirst("^confidence_", "")
										+ (modelName == null
												|| modelName.isEmpty() ? ""
												: ":" + modelName),
								examples.getExample(0).getValue(
										role.getAttribute()));
					}

				}
			}

		} catch (Exception e) {
			log.error("Failed to apply model: {}", e.getMessage());
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
		}

		return input;
	}

	/**
	 * @return the model
	 */
	public SourceURL getModel() {
		return model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(SourceURL model) {
		this.model = model;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Boolean getConfidence() {
		return confidence;
	}

	public void setConfidence(Boolean confidence) {
		this.confidence = confidence;
	}

}