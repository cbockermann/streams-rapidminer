/**
 * 
 */
package stream.mining;

import java.io.Serializable;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class PredictionError extends AbstractProcessor {

	Double errors = 0.0d;

	String label = "@label";
	String prediction = "@prediction";

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable labelValue = input.get(label);
		Serializable predValue = input.get(prediction);

		if (labelValue != null && predValue != null) {

			if (labelValue == predValue || labelValue.equals(predValue)) {

			} else {
				errors += 1.0d;
			}

			input.put("@prediction:error", errors);
		}

		return input;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the prediction
	 */
	public String getPrediction() {
		return prediction;
	}

	/**
	 * @param prediction
	 *            the prediction to set
	 */
	public void setPrediction(String prediction) {
		this.prediction = prediction;
	}

}