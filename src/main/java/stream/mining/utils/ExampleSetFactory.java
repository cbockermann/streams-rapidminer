/**
 * 
 */
package stream.mining.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.example.table.DataRowFactory;
import com.rapidminer.example.table.MemoryExampleTable;
import com.rapidminer.tools.Ontology;
import com.rapidminer.tools.att.AttributeSet;

/**
 * <p>
 * This class provides utility methods for converting a collection of data items
 * into a RapidMiner example set.
 * </p>
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 */
public class ExampleSetFactory {

	static Logger log = LoggerFactory.getLogger(ExampleSetFactory.class);

	/**
	 * <p>
	 * This method creates an example set based on a given collection of data
	 * items. Each data item is transformed into a single row of that data item.
	 * The ordering of the collection determines the ordering of the resulting
	 * rows of the example set.
	 * </p>
	 * <p>
	 * All numerical features of the data item are converted into <i>real
	 * valued</i> attributes of the example set, any other feature is mapped to
	 * a <i>nominal</i> attribute.
	 * </p>
	 * <p>
	 * Based on the <code>@..</code> syntax inherent to <i>streams</i> data
	 * items, any feature starting with an <code>@</code> character (i.e.
	 * <i>annotations</i>) is mapped to a <i>special</i> attribute. That is, a
	 * feature named <code>@id</code> will be mapped to a RapidMiner ID
	 * attribute role and any feature, which's key starts with
	 * <code>@label</code> will be mapped to the LABEL attribute role.
	 * </p>
	 * <p>
	 * <b>Important:</b> If there are multiple features that are named with a
	 * <code>@label</code> prefix, then the <i>last</i> one of these will become
	 * the LABEL role attribute in the resulting RapidMiner example set.
	 * </p>
	 * 
	 * @param items
	 *            The collection of data items that should be mapped to
	 *            examples.
	 * @return An ExampleSet object that represents the collection of data
	 *         items.
	 */
	public static ExampleSet createExampleSet(Collection<Data> items) {

		Map<String, Class<?>> attributes = new LinkedHashMap<String, Class<?>>();

		for (Data item : items) {

			for (String key : item.keySet()) {

				Serializable s = item.get(key);
				if (Number.class.isAssignableFrom(s.getClass())) {
					attributes.put(key, Double.class);
				} else {
					attributes.put(key, String.class);
				}
			}
		}

		log.debug("Incoming data stream contains {} examples", items.size());

		AttributeSet columns = new AttributeSet();
		Attribute[] attributeArray = new Attribute[attributes.size()];

		MemoryExampleTable table = new MemoryExampleTable();
		int i = 0;
		for (String key : attributes.keySet()) {

			int type = Ontology.NUMERICAL;

			if (String.class.equals(attributes.get(key))) {
				type = Ontology.NOMINAL;
			}

			if (Double.class.equals(attributes.get(key))
					|| Integer.class.equals(attributes.get(key))) {
				type = Ontology.REAL;
			}

			Attribute attr = AttributeFactory.createAttribute(key, type);
			columns.addAttribute(attr);
			table.addAttribute(attr);
			attributeArray[i++] = attr;
		}

		DataRowFactory drf = new DataRowFactory(
				DataRowFactory.TYPE_DOUBLE_ARRAY, '.');

		for (Data datum : items) {
			String[] data = new String[attributeArray.length];
			i = 0;
			for (String key : attributes.keySet()) {

				if (datum.get(key) == null)
					data[i] = "?";
				else
					data[i] = datum.get(key).toString();
				i++;
			}

			while (i < attributeArray.length)
				data[i++] = "?";

			table.addDataRow(drf.create(data, attributeArray));
		}

		ExampleSet exampleSet = table.createExampleSet();

		// Attributes attributeSet = exampleSet.getAttributes();
		List<Attribute> attributeSet = new ArrayList<Attribute>();
		for (Attribute attr : exampleSet.getAttributes())
			attributeSet.add(attr);

		for (Attribute attr : attributeSet) {
			if (attr.getName().startsWith("@id")) {
				exampleSet.getAttributes().setId(attr);
				continue;
			}

			if (attr.getName().startsWith("@label")) {
				exampleSet.getAttributes().setLabel(attr);
				continue;
			}

			if (attr.getName().startsWith("@")) {
				exampleSet.getAttributes().remove(attr);
			}
		}

		return exampleSet;
	}
}