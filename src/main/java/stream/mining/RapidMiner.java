/**
 * 
 */
package stream.mining;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rapidminer.RapidMiner.ExecutionMode;
import com.rapidminer.io.Base64.InputStream;
import com.rapidminer.tools.XMLException;

/**
 * @author cris, hendrik
 * 
 */
public class RapidMiner {

	final static Logger log = LoggerFactory.getLogger(RapidMiner.class);

	final static AtomicBoolean initialized = new AtomicBoolean(false);

	public static void initialize() {
		synchronized (initialized) {

			if (initialized.get()) {
				log.info("RapidMiner library has already been initialized.");
				return;
			} else {
				log.info("Initializing RapidMiner...");
				com.rapidminer.RapidMiner.init();
				initialized.set(true);
			}
		}
	}

	public static void initialize(ExecutionMode executionMode) {
		synchronized (initialized) {

			if (initialized.get()) {
				log.info("RapidMiner library has already been initialized.");
				return;
			} else {
				log.info("Initializing RapidMiner...");
				com.rapidminer.RapidMiner.setExecutionMode(executionMode);
				com.rapidminer.RapidMiner.init();
				initialized.set(true);
			}
		}
	}

	public static com.rapidminer.Process readProcessFile(File file)
			throws InstantiationException, IllegalAccessException,
			XMLException, IOException {
		return com.rapidminer.RapidMiner.readProcessFile(file);

	}

	public static com.rapidminer.Process createProcess(InputStream in)
			throws InstantiationException, IllegalAccessException,
			XMLException, IOException {
		return new com.rapidminer.Process(in);
	}
}