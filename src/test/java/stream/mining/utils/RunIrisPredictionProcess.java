/**
 * 
 */
package stream.mining.utils;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class RunIrisPredictionProcess {

	static Logger log = LoggerFactory.getLogger(RunIrisPredictionProcess.class);

	@Test
	public void testProcess() throws Exception {

		URL url = RunIrisPredictionProcess.class
				.getResource("/iris-prediction.xml");
		stream.run.main(url);

	}
}