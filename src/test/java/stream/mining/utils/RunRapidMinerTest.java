/**
 * 
 */
package stream.mining.utils;

import static org.junit.Assert.fail;

import java.net.URL;
import java.util.UUID;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.expressions.version2.SourceURLExpression;
import stream.expressions.version2.StringArrayExpression;
import stream.expressions.version2.StringExpression;
import stream.io.CsvStream;
import stream.io.SourceURL;
import stream.mining.ApplyModel;
import stream.mining.ExecuteProcess;
import stream.runtime.ProcessContextImpl;

/**
 * @author Hendrik Blom
 * 
 */
public class RunRapidMinerTest {

	static Logger log = LoggerFactory.getLogger(RunRapidMinerTest.class);

	@Test
	public void test() {
		try {
			ExecuteProcess exe = new ExecuteProcess();
			Data macros = DataFactory.create();
			macros.put("process.uid", UUID.randomUUID().toString());

			macros.put("model.features",
					"X,Y,month,day,FFMC,DMC,DC,ISI,temp,RH,wind,rain");

			URL url = RunRapidMinerProcessTest.class.getResource("/");
			macros.put("model.output", url.getPath());
			url = RunRapidMinerProcessTest.class
					.getResource("/forestfires.csv");

			macros.put("data.file", url.getPath());
			macros.put("model.id",
					"forestfires-model-+" + macros.get("process.uid") + ".xml");
			macros.put("model.label", "area");

			macros.put("process.url", "classpath:/NuSVR6_test.rm");

			String keys = "process.id,data.file,model.label,model.features,model.id,model.output";
			exe.setMacros(new StringArrayExpression(keys));
			exe.setProcessUrl(new SourceURLExpression(
					"classpath:/NuSVR6_test.rm"));

			exe.setPid(new StringExpression(macros.get("process.uid")
					.toString()));

			exe.init(null);

			// model1
			exe.process(macros);

			// macros.put("process_uid", "puid2");
			// exe.process(macros);
			//
			// macros.put("process_uid", "puid3");
			// exe.process(macros);
			//
			// macros.put("process_uid", "puid1");
			// applyModel(macros);
			// macros.put("process_uid", "puid2");
			// applyModel(macros);
			// macros.put("process_uid", "puid3");
			// applyModel(macros);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Test failed: " + e.getMessage());
		}
	}

	private void applyModel(Data macros) throws Exception {
		ApplyModel apply = new ApplyModel();

		SourceURL url = new SourceURL("file:" + macros.get("model_output")
				+ macros.get("model_name") + "_" + macros.get("process_uid")
				+ "." + macros.get("model_extension"));

		apply.setModel(url);
		apply.setConfidence(false);
		apply.setModelName("test");
		apply.init(new ProcessContextImpl());

		CsvStream stream = new CsvStream(new SourceURL(
				"classpath:/forestfires.csv"));

		stream.init();

		long start = System.currentTimeMillis();
		Data item = stream.read();
		int count = 0;
		int c = 0;
		int id = 0;
		while (item != null) {
			item.put("@id", id);
			System.out.println(id);
			id++;
			// log.info("Applying model to {}", item);
			apply.process(item);
			c++;
			item = stream.read();
			if (item == null && count < 20) {
				System.out.println(count);
				count++;
				id = 0;
				stream.close();
				stream.init();
				item = stream.read();
			}
			// log.info("item after application:  {}", item);
		}
		long stop = System.currentTimeMillis();
		System.out.println(c + ":" + (stop - start));
		stream.close();
	}
}