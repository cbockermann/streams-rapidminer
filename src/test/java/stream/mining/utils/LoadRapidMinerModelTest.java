/**
 * 
 */
package stream.mining.utils;

import static org.junit.Assert.fail;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.CsvStream;
import stream.io.SourceURL;
import stream.mining.ApplyModel;
import stream.runtime.ProcessContextImpl;

/**
 * @author chris
 * 
 */
public class LoadRapidMinerModelTest {

	static Logger log = LoggerFactory.getLogger(LoadRapidMinerModelTest.class);

	@Test
	public void test() {
		try {

			ApplyModel apply = new ApplyModel();

			SourceURL url = new SourceURL("classpath:/mydt.obj");

			apply.setModel(url);
			apply.setConfidence(true);
			apply.setModelName("test");
			apply.init(new ProcessContextImpl());

			CsvStream stream = new CsvStream(new SourceURL(
					"classpath:/iris.csv"));

			stream.init();

			long start = System.currentTimeMillis();
			Data item = stream.read();
			int count = 0;
			int c = 0;
			while (item != null) {
				// log.info("Applying model to {}", item);
				apply.process(item);
				c++;
				item = stream.read();
				if (item == null && count < 20) {
					count++;
					stream.close();
					stream.init();
					item = stream.read();
				}
				// log.info("item after application:  {}", item);
			}
			long stop = System.currentTimeMillis();
			System.out.println(c + ":" + (stop - start));
			stream.close();

		} catch (Exception e) {
			e.printStackTrace();
			fail("Test failed: " + e.getMessage());
		}
	}
}