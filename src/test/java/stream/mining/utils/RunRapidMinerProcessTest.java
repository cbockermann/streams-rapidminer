/**
 * 
 */
package stream.mining.utils;

import java.net.URL;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Hendrik
 * 
 */
public class RunRapidMinerProcessTest {

	static Logger log = LoggerFactory.getLogger(RunRapidMinerProcessTest.class);

	@Test
	public void testProcess() throws Exception {

		URL url = RunRapidMinerProcessTest.class
				.getResource("/forestfires-learnmodel.xml");
		stream.run.main(url);

	}
}