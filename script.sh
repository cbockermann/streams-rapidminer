#!/bin/bash 
function mvninst {
	mvn install:install-file -Dfile=$2.jar -DgroupId=$1 -DartifactId=$2 -Dversion=$3 -Dpackaging=jar
}  
mvninst rapidminer rapidminer 5.3
mvninst rapidminer launcher 5.3
#
mvninst SassyReader SassyReader 0.5
mvninst encog encog 1.0
mvninst jama jama 1.0
mvninst jep jep 1.0
mvninst joone-engine joone-engine 1.0
mvninst jxl jxl 1.0
mvninst poi poi 3.8
mvninst poi poi-ooxml 3.8
mvninst poi poi-ooxml-schemas 3.8
mvninst poi poi-scratchpad 3.8
mvninst vldocking vldocking 1.0
mvninst xmlpull xmlpull 1.0
mvninst xpp3 xpp3 1.0
mvninst xstream xstream 1.0

